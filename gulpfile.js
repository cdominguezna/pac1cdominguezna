var gulp         = require('gulp'),
    del          = require('del'),
    browserSync  = require('browser-sync').create(),
    concat       = require('gulp-concat'),
    imagemin     = require('gulp-imagemin'),
    jshint       = require('gulp-jshint'),
    stylish      = require('jshint-stylish'),
    plumber      = require('gulp-plumber'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    uglify       = require('gulp-uglify'),
    uglifycss    = require('gulp-uglifycss'),
    gulpSequence = require('gulp-sequence');


// Definición de directorios origen
var srcPaths = {
    images:    'src/img/',
    scripts:   'src/js/',
    styles:    'src/scss/',
    vendor:  'src/vendor/',
    files:     'src/',
    data:      'src/data/'
};


// Definición de directorios destino
var distPaths = {
    images:    'dist/img/',
    scripts:   'dist/js/',
    styles:    'dist/css/',
    vendor:    'dist/vendor/',
    files:     'dist/',
    data:      'dist/data/'
};


gulp.task('vendor', function() {
    return gulp.src([srcPaths.vendor+'**/*'])
        .pipe(gulp.dest(distPaths.vendor))
        .pipe(browserSync.stream());
});

gulp.task('data', function() {
    return gulp.src([srcPaths.data+'**/*'])
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
});

gulp.task('html', function() {
    return gulp.src([srcPaths.files+'*.html'])
        .pipe(gulp.dest(distPaths.files))
        .pipe(browserSync.stream());
});

// Etapa4 | Creació de la tasca 'clean' 
//Es crea la tasca clean per la neteja de la carpeta 'dist'
gulp.task('clean', function(cb) {
    return del([ distPaths.files+'**/*'], cb);
});


// Etapa5 | Creació de la tasca 'copy' 
//Es crea una tasca 'copy' de còpia directa d'arxius / carpetes
// index.html = html, dist/vendor = vendor y dist/data = data.
gulp.task('copy', ['html', 'vendor', 'data'], function() {});


// Etapa6 | Creació de la tasca 'imagemin' 
// Aquesta tasca recupera les imatges font, les comprimeix i les copia a la carpeta dist/img
gulp.task('imagemin', function() {
    return gulp.src([srcPaths.images+'**/*'])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});


// Etapa7 | Creació de la tasca 'scss' 
// Aquesta tasca s'ocupara de la transpilació del scss a css.
gulp.task('scss', function() {
    return gulp.src([srcPaths.styles+'**/base.scss'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream());
});

// Etapa8 | Creació de la tasca 'lint'
//Aquesta tasca gràcies al mòdul gulp-jshint revisarà el codi font javascript.
gulp.task('lint', function() {
    return gulp.src([srcPaths.scripts+'**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

// Etapa9 | Creació de la tasca 'js'
//Aquesta tasca executarà inicialment la tasca lint per revisar el codi js i desprès l'optimitzara.
gulp.task('js', ['lint'], function() {
    return gulp.src([srcPaths.scripts+'contact.js', srcPaths.scripts+'contactList.js', srcPaths.scripts+'main.js'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream());
});

// Etapa10 | Creació de la tasca 'serve'
//Aquesta tasca crea un build per visualitzar l'aplicació, i utilitzarà browsersync per visualitzarla en diferents dispositius,
// A més monitoritizarà qualsevol canvi fets en els fitxer, llençant un refresc automàtic
gulp.task('serve', ['copy', 'imagemin', 'scss', 'js'], function() {
    browserSync.init({
        logLevel: "info",
        browser: ["chrome","firefox"],
        server: distPaths.files,
        startPath: ''
    });

    gulp.watch(srcPaths.files+'*.html', ['html']);
    gulp.watch(srcPaths.vendor+'**/*', ['vendor']);
    gulp.watch(srcPaths.data+'**/*', ['data']);
    gulp.watch(srcPaths.images+'**/*', ['imagemin']);
    gulp.watch(srcPaths.styles+'**/*.scss', ['css']);
    gulp.watch(srcPaths.scripts+'**/*.js', ['js']);

});  


//Etapa11 -- Modificació incluint la crida al server
// Definición de tasca per defecte
gulp.task('default', function(cb) {
    gulpSequence('clean','serve',cb);
});


//Etapa opcional - 13 : Ús de l'entorn de XAMPP que tinc instalat.
gulp.task('serveXAMPP', ['copy', 'imagemin', 'scss', 'js'], function() {
    browserSync.init({
        logLevel: "info",
        browser: ["chrome","firefox"],
        proxy: "localhost:80",
        startPath: "/contacts-agenda/dist/"
    });

    gulp.watch(srcPaths.files+'*.html', ['html']);
    gulp.watch(srcPaths.vendor+'**/*', ['vendor']);
    gulp.watch(srcPaths.data+'**/*', ['data']);
    gulp.watch(srcPaths.images+'**/*', ['imagemin']);
    gulp.watch(srcPaths.styles+'**/*.scss', ['css']);
    gulp.watch(srcPaths.scripts+'**/*.js', ['js']);

});